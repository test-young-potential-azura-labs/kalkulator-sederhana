function kalkulator(angka1, operator, angka2) {
  if (angka1 <= 1000000 && angka2 <= 1000000) {
    if (operator == '+') {
      console.log(Number(angka1) + Number(angka2));
    } else if (operator == '-') {
      console.log(Number(angka1) - Number(angka2));
    } else if (operator == '*') {
      console.log(Number(angka1) * Number(angka2));
    } else if (operator == '/') {
      console.log(Number(angka1) / Number(angka2));
    }
  } else {
    console.log('error');
  }
}
kalkulator('13', '+', '187');
kalkulator('134', '-', '11');
kalkulator('8', '*', '7');
kalkulator('16', '/', '4');
